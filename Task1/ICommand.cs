﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    interface ICommand
    {
        void Execute();
        //возможно последующее добавление
        void UnDo();
        void ReDo();
    }
}
