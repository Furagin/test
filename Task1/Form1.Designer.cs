﻿namespace Task1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchFileButton = new System.Windows.Forms.Button();
            this.startDirectoryBox = new System.Windows.Forms.TextBox();
            this.patternBox = new System.Windows.Forms.TextBox();
            this.labelDirectory = new System.Windows.Forms.Label();
            this.labelPattern = new System.Windows.Forms.Label();
            this.textFileBox = new System.Windows.Forms.TextBox();
            this.labelText = new System.Windows.Forms.Label();
            this.InfoButton = new System.Windows.Forms.Button();
            this.labelPreText = new System.Windows.Forms.Label();
            this.TreeFilePath = new System.Windows.Forms.TreeView();
            this.labelFilesTree = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.selectedFileName = new System.Windows.Forms.Label();
            this.pauseButton = new System.Windows.Forms.Button();
            this.selectDirectoryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SearchFileButton
            // 
            this.SearchFileButton.Location = new System.Drawing.Point(380, 361);
            this.SearchFileButton.Name = "SearchFileButton";
            this.SearchFileButton.Size = new System.Drawing.Size(408, 33);
            this.SearchFileButton.TabIndex = 0;
            this.SearchFileButton.Text = "Поиск файла";
            this.SearchFileButton.UseVisualStyleBackColor = true;
            this.SearchFileButton.Click += new System.EventHandler(this.SearchFileButton_Click);
            // 
            // startDirectoryBox
            // 
            this.startDirectoryBox.Location = new System.Drawing.Point(557, 13);
            this.startDirectoryBox.Name = "startDirectoryBox";
            this.startDirectoryBox.Size = new System.Drawing.Size(231, 22);
            this.startDirectoryBox.TabIndex = 1;
            // 
            // patternBox
            // 
            this.patternBox.Location = new System.Drawing.Point(557, 71);
            this.patternBox.Name = "patternBox";
            this.patternBox.Size = new System.Drawing.Size(231, 22);
            this.patternBox.TabIndex = 2;
            // 
            // labelDirectory
            // 
            this.labelDirectory.AutoSize = true;
            this.labelDirectory.Location = new System.Drawing.Point(377, 16);
            this.labelDirectory.Name = "labelDirectory";
            this.labelDirectory.Size = new System.Drawing.Size(163, 17);
            this.labelDirectory.TabIndex = 3;
            this.labelDirectory.Text = "Начальная директория";
            // 
            // labelPattern
            // 
            this.labelPattern.AutoSize = true;
            this.labelPattern.Location = new System.Drawing.Point(377, 74);
            this.labelPattern.Name = "labelPattern";
            this.labelPattern.Size = new System.Drawing.Size(137, 17);
            this.labelPattern.TabIndex = 4;
            this.labelPattern.Text = "Шаблон для поиска";
            // 
            // textFileBox
            // 
            this.textFileBox.Location = new System.Drawing.Point(557, 99);
            this.textFileBox.Multiline = true;
            this.textFileBox.Name = "textFileBox";
            this.textFileBox.Size = new System.Drawing.Size(231, 115);
            this.textFileBox.TabIndex = 5;
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(377, 102);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(104, 17);
            this.labelText.TabIndex = 6;
            this.labelText.Text = "Текст в файле";
            // 
            // InfoButton
            // 
            this.InfoButton.Location = new System.Drawing.Point(380, 316);
            this.InfoButton.Name = "InfoButton";
            this.InfoButton.Size = new System.Drawing.Size(408, 39);
            this.InfoButton.TabIndex = 8;
            this.InfoButton.Text = "Информация";
            this.InfoButton.UseVisualStyleBackColor = true;
            this.InfoButton.Click += new System.EventHandler(this.InfoButton_Click);
            // 
            // labelPreText
            // 
            this.labelPreText.AutoSize = true;
            this.labelPreText.Location = new System.Drawing.Point(377, 234);
            this.labelPreText.Name = "labelPreText";
            this.labelPreText.Size = new System.Drawing.Size(134, 17);
            this.labelPreText.TabIndex = 11;
            this.labelPreText.Text = "Файл в обработке:";
            // 
            // TreeFilePath
            // 
            this.TreeFilePath.Location = new System.Drawing.Point(12, 41);
            this.TreeFilePath.Name = "TreeFilePath";
            this.TreeFilePath.Size = new System.Drawing.Size(342, 390);
            this.TreeFilePath.TabIndex = 13;
            // 
            // labelFilesTree
            // 
            this.labelFilesTree.AutoSize = true;
            this.labelFilesTree.Location = new System.Drawing.Point(13, 15);
            this.labelFilesTree.Name = "labelFilesTree";
            this.labelFilesTree.Size = new System.Drawing.Size(137, 17);
            this.labelFilesTree.TabIndex = 14;
            this.labelFilesTree.Text = "Найденные файлы:";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(380, 287);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(408, 23);
            this.progressBar1.TabIndex = 15;
            // 
            // selectedFileName
            // 
            this.selectedFileName.AutoSize = true;
            this.selectedFileName.Location = new System.Drawing.Point(384, 261);
            this.selectedFileName.Name = "selectedFileName";
            this.selectedFileName.Size = new System.Drawing.Size(0, 17);
            this.selectedFileName.TabIndex = 16;
            // 
            // pauseButton
            // 
            this.pauseButton.Location = new System.Drawing.Point(380, 401);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(408, 30);
            this.pauseButton.TabIndex = 17;
            this.pauseButton.Text = "Пауза в поиске";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // selectDirectoryButton
            // 
            this.selectDirectoryButton.Location = new System.Drawing.Point(557, 42);
            this.selectDirectoryButton.Name = "selectDirectoryButton";
            this.selectDirectoryButton.Size = new System.Drawing.Size(231, 23);
            this.selectDirectoryButton.TabIndex = 19;
            this.selectDirectoryButton.Text = "Выбор директории";
            this.selectDirectoryButton.UseVisualStyleBackColor = true;
            this.selectDirectoryButton.Click += new System.EventHandler(this.SelectDirectoryButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 443);
            this.Controls.Add(this.selectDirectoryButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.selectedFileName);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.labelFilesTree);
            this.Controls.Add(this.TreeFilePath);
            this.Controls.Add(this.labelPreText);
            this.Controls.Add(this.InfoButton);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.textFileBox);
            this.Controls.Add(this.labelPattern);
            this.Controls.Add(this.labelDirectory);
            this.Controls.Add(this.patternBox);
            this.Controls.Add(this.startDirectoryBox);
            this.Controls.Add(this.SearchFileButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SearchFileButton;
        private System.Windows.Forms.TextBox startDirectoryBox;
        private System.Windows.Forms.TextBox patternBox;
        private System.Windows.Forms.Label labelDirectory;
        private System.Windows.Forms.Label labelPattern;
        private System.Windows.Forms.TextBox textFileBox;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Button InfoButton;
        private System.Windows.Forms.Label labelPreText;
        private System.Windows.Forms.TreeView TreeFilePath;
        private System.Windows.Forms.Label labelFilesTree;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label selectedFileName;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button selectDirectoryButton;
    }
}

