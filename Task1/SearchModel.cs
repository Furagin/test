﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Task1
{
    class SearchModel
    {
        private string researchDirectory;
        private string patternFile;
        private string sampleInFile;
        private string[] arrayFile;

        private Label labelFileName;

        private List<string> foundFile = new List<string>();
        public SearchModel(ref Label fileNameLabel)
        {
            researchDirectory = System.IO.Directory.GetCurrentDirectory();
            patternFile = "*.txt";
            sampleInFile = "";
            labelFileName = fileNameLabel;
        }
        public string GetDirectory()
        {
            return researchDirectory;
        }

        public void SetDirectory(string directory)
        {
            researchDirectory = directory;
        }
        public void SetPattern(string pattern)
        {
            patternFile = pattern;
        }
        public string GetPattern()
        {
            return patternFile;
        }
        public void SetSample(string sample)
        {
            sampleInFile = sample;
        }

        public string GetSample()
        {
            return sampleInFile;
        }

        public List<string> GetResult()
        {
            return foundFile;
        }
        public void Search()
        {
            try
            {
                arrayFile = Directory.GetFiles(researchDirectory, patternFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }

            foreach (string currentFile in arrayFile)
            {
                SetLabelInForm(currentFile);
                if (SearchInFile(currentFile, sampleInFile))
                {
                    foundFile.Add(currentFile);
                }
            }
        }
        
        //поиск текста в файле
        private bool SearchInFile(string pathToFile, string sample)
        {
            bool resultResearch = false;
            //
            string textCurrentFile = File.ReadAllText(pathToFile);
            if (textCurrentFile.IndexOf(sample) >= 0)
            {
                resultResearch = true;
            }
            //
            return resultResearch;
        }

        private void SetLabelInForm(string fileName)
        {
            labelFileName.Invoke(new Action(() => { labelFileName.Text = fileName;}));
            //Задержка проверки
            Thread.Sleep(1000);
        }
    }
}
