﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class CommandManager
    {
        private static CommandManager instance;
        public string Value { get; set; }
        private static readonly object LockCm = new object();


        private LinkedList<ICommand> commands = new LinkedList<ICommand>();
        private LinkedListNode<ICommand> currentCommand;

        private CommandManager(string value)
        {
            this.Value = value;
        }

        public static CommandManager GetInstance(string valueN)
        {
            if (instance == null)
            {
                lock (LockCm)
                {
                    if (instance == null)
                    {
                        instance = new CommandManager("Exist");
                    }
                }
            }

            return instance;
        }

        public void ExecuteCommand(ICommand command)
        {
            if (currentCommand?.Next != null)
            {
                while (currentCommand != commands.Last)
                {
                    commands.RemoveLast();
                }
            }
            commands.AddLast(command);
            currentCommand = commands.Last;
            currentCommand.Value.Execute();
        }

        public void UnDo()
        {
            if (currentCommand != null)
            {
                currentCommand.Value.UnDo();
                currentCommand = currentCommand.Previous;
            }
        }

        public void Redo()
        {
            if (currentCommand.Next != null)
            {
                currentCommand.Value.ReDo();
                currentCommand = currentCommand.Next;
            }
        }
    }
}
