﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class SearchCommand: ICommand
    {
        private SearchModel model;
        public SearchCommand(SearchModel model, string directory, string pattern, string sample)
        {
            this.model = model;
            this.model.SetDirectory(directory);
            this.model.SetPattern(pattern);
            this.model.SetSample(sample);
        }
        public void Execute()
        {
            model.Search();
            model.GetResult().ToArray();
            //передача инфо в DataSaver
        }
        public void ReDo()
        {
            throw new NotImplementedException();
        }
        public void UnDo()
        {
            throw new NotImplementedException();
        }
    }
}
