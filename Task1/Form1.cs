﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task1.Commands;

namespace Task1
{
    public partial class Form1 : Form
    {
        CommandManager Manager;  
        private static ManualResetEvent mre = new ManualResetEvent(false);

        private SearchModel model;

        private Thread newSearchThread;
        //делегат для изменения названия из потока рассмотрения файла
        public delegate void SetCurrentFileName(Label label, string text);
        public Form1()
        {
            InitializeComponent();
            model = new SearchModel(ref selectedFileName);
            //Manager.GetInstance("Exist");
        }
        private void SearchFileButton_Click(object sender, EventArgs e)
        {
            SetSearchCharacters();
            //StartSearch();
            Manager.ExecuteCommand(new SearchCommand(model, model.GetDirectory(),model.GetPattern(), model.GetSample()));
        }
        private void InfoButton_Click(object sender, EventArgs e)
        {
            Form2 frmInfo = new Form2();
            frmInfo.Show();
        }
        private void PauseButton_Click(object sender, EventArgs e)
        {
            if (pauseButton.Text == "Пауза в поиске")
            {
                /*mre.Set();
                mre.WaitOne(Timeout.Infinite);
                pauseButton.Text = "Возобновить поиск";*/
                Manager.ExecuteCommand(new PauseCommand());
            }
            else
            {
                /*mre.Reset();
                pauseButton.Text = "Пауза в поиске";*/
                Manager.ExecuteCommand(new ResumeCommand());
            }
        }
        private void SetSearchCharacters()
        {
            try
            {
                model.SetDirectory(startDirectoryBox.Text);
                model.SetPattern(patternBox.Text);
                model.SetSample(textFileBox.Text);
            }
            catch 
            {
                Form2 problemForm = new Form2("Problem with parameters.");
                problemForm.Show();
            }
        }

        /*private void StartSearch()
        {
            //Запуск поиска в отдельном потоке
            newSearchThread = new Thread(new ThreadStart(model.Search));
            newSearchThread.Start();
        }*/
        private void SelectDirectoryButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog newDirectory = new FolderBrowserDialog();
            newDirectory.ShowDialog();
            string selectedPath = newDirectory.SelectedPath;
            model.SetDirectory(selectedPath);
            if (selectedPath != "") startDirectoryBox.Text = selectedPath;
        }
    }
}
