﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task1
{
    class DataSaver
    {
        private static DataSaver instance;
        private static readonly object LockCm = new object();

        private string[] foundedFiles = new string[] { };
        private List<string> files = new List<string>();

        private Label labelFileName;
        private TreeView treeFileSet;

        private static DataSaver GetInstance(ref Label labelName, ref TreeView treeFile)
        {
            if (instance == null)
            {
                lock (LockCm)
                {
                    if (instance == null)
                    {
                        instance = new DataSaver();
                        instance.labelFileName = labelName;
                    }
                }
            }

            return instance;
        }

        public void AddNewFile(string fileName)
        {
            ShowFileName(fileName);
            files.Add(fileName);
            foundedFiles = files.ToArray();
        }

        private void ShowFileName(string fileName)
        {
            //отправлка в Label на форме
            labelFileName.Invoke(new Action(() => { labelFileName.Text = fileName; }));
            //Задержка проверки (потом следует убрать)
            Thread.Sleep(1000);
        }

        public void UpdateTree(string[] foundFiles)
        {
            //обновление дерева
        }
    }
}
